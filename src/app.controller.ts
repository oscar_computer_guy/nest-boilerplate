import { Controller, Get, HttpException, HttpStatus } from "@nestjs/common";
import { AppService } from "./app.service";

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get("unhandled_error")
  getUnhandled(): string {
    throw new Error("I am an unhandled error");
    return "I am not going to be returned and you know it";
  }

  @Get("normal_error")
  getNormalError(): string {
    throw new HttpException("Normal error here", HttpStatus.FORBIDDEN);
    return "I am not going to be returned and you know it";
  }
}
