import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import * as bodyParser from "body-parser";
import { EnvVars } from "./common/getEnv";

import { AllExceptionsFilter } from "./common/exceptions/general.filter";
import { LoggingInterceptor } from "./common/logging/logging.interceptor";
import { RateLimiterMiddleware } from "./common/middleware/rateLimiterRedis";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalFilters(new AllExceptionsFilter());
  app.useGlobalInterceptors(new LoggingInterceptor());
  app.use(bodyParser.json({ limit: "5mb" }));
  app.use(bodyParser.urlencoded({ limit: "5mb", extended: true }));
  app.use(RateLimiterMiddleware); // comment this out if you are running a load test
  app.enableCors();

  const Port = EnvVars.PORT || 8080;
  await app.listen(Port, () => {
    console.log(`app running on port.... ${Port}`);
  });
}
bootstrap();
