import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
  Logger,
} from "@nestjs/common";

import { logger } from "../logging/logger";

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    let status: number;

    if (exception instanceof HttpException) {
      status = 400;
      logger.warn(exception);
    } else {
      logger.error({ message: exception.message, data: exception.stack });
      status = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    response.status(status).json({
      message: "We can put whatever we want here",
    });
  }
}
