import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from "@nestjs/common";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const args = context.getArgs();

    const { url, method, headers, body } = args[0];
    const request = context.switchToHttp().getRequest();

    request.body.brandId = headers.brand_id;

    return next.handle();
  }
}
